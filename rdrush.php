<?php

/**
 * @file
 * Remote Drush enables the user to execute drush commands on remote websites.
 */

// Config.
$config = array(
  "cache_file" => "/tmp/rdrush.txt",
  "drupal_cookie_file" => "/tmp/drupal_cookie.txt",
);

// If no command is found, display help.
if (empty($argv[1])) {
  help();
}
// Execute the command if it exists.
else {
  $func = str_replace("-", "_", $argv[1]);
  if (function_exists($func)) {
    call_user_func($func, $argv);
  }
  else {
    print "The rdrush command '" . $argv[1] . "' could not be found. Run `rdrush.php help` to view all available commands.\n";
  }
}

/**
 * Display version and author information.
 */
function __version() {
  print "Remote Drush Version: 0.1\n";
  print "Author: Patrick Scheffer\n";
  print "Based on Drush (http://www.drush.org) originally developed by Arto Bendiken.\n";
}

/**
 * Display help information.
 */
function help() {
  print "Execute a drush command on a remote drupal website without the need to connect to the server.\n\n";
  print "Core rdrush commands: (core)\n";
  print " connect                   Set up a connection to a remote drupal website. Don't forget to disconnect when done to delete session files.\n";
  print "  Arguments:\n";
  print "   Base URL                The base URL of the drupal website to connect with, e.g. http://www.example.com.\n";
  print "   Drupal username         The username of the Drupal account on the remote website. Make sure this user has admin rights.\n";
  print "   Drupal password         The password of the Drupal account on the remote website.\n";
  print "   HTPASSWD username       Optional username for basic htaccess authentication on the remote environment.\n";
  print "   HTPASSWD password       Optional password for basic htaccess authentication on the remote environment.\n";
  print " disconnect                Destroy the connection with the remote drupal website.\n";
  print " updatedb (updb)           Apply any database updates required (as with running update.php).\n";
  print "Cache commands: (cache)\n";
  print " cache-clear (cc)          Clear all drupal caches.\n";
  print "Features commands: (features)\n";
  print " feature-revert-all (fra)  Revert all enabled feature module on your site.\n";
}

/**
 * Create a connection to a remote environment.
 *
 * @param array $argv
 *   Arguments passed to this script.
 *
 * @return bool
 *   TRUE if the connection succeeded, otherwise FALSE.
 */
function connect($argv) {
  // Check if the required arguments are set.
  if (empty($argv[4])) {
    print "Please provide a base URL, username and password to connect to.\n";
    return FALSE;
  }

  // Get the arguments.
  $base_url = $argv[2];
  $drupal_user = $argv[3];
  $drupal_pass = $argv[4];

  // Create a cache array.
  $cache = array(
    'base_url' => $base_url,
  );

  if (!empty($argv[6])) {
    $cache['http_user'] = $argv[5];
    $cache['http_pass'] = $argv[6];
  }

  // Save the arguments to a cache file.
  if (!cache_set($cache)) {
    print "Error saving cache.\n";
    return FALSE;
  }

  // Login to drupal.
  print "Setting up connection...\n";
  $result = do_request($base_url . "/user/login", "name=$drupal_user&pass=$drupal_pass&form_id=user_login");
  // Look for an error message.
  if (preg_match('@\<div class="alert-block alert alert-error"\>@', $result, $matches)) {
    print "Error logging in.";
    return FALSE;
  }

  print "Connected.\n";
  return TRUE;
}

/**
 * Remove the temporary session files.
 */
function disconnect() {
  global $config;

  if (file_exists($config['drupal_cookie_file'])) {
    unlink($config['drupal_cookie_file']);
  }
  if (file_exists($config['cache_file'])) {
    unlink($config['cache_file']);
  }

  print "Disconnected.\n";
}

/**
 * Check if the session files exist.
 *
 * @return bool
 *   TRUE if the session files exist, otherwise FALSE.
 */
function check_connection() {
  global $config;

  if (!file_exists($config['drupal_cookie_file']) || !file_exists($config['cache_file'])) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Save an array serialized to a file.
 *
 * @param array $data
 *   Data to save.
 *
 * @return int
 *   The number of bytes that were written to the file, or FALSE on failure.
 */
function cache_set($data) {
  global $config;

  return file_put_contents($config['cache_file'], serialize($data));
}

/**
 * Load data from the cache.
 *
 * @param string $key
 *   An optional key to extract from the cached data.
 *
 * @return bool|array|string
 *   Cached data as an array, a string if $key is set or FALSE on error or
 *   nothing is found.
 */
function cache_get($key = "") {
  global $config;

  // Check if the cache file exists.
  if (!file_exists($config['cache_file'])) {
    return FALSE;
  }

  // Load and unserialize its contents.
  $data = file_get_contents($config['cache_file']);
  $data = unserialize($data);

  // Look for the key (and return it) if given.
  if (!empty($key)) {
    if (isset($data[$key])) {
      return $data[$key];
    }
    return FALSE;
  }

  // Return the unserialized data.
  return $data;
}

/**
 * Alias function for @updatedb.
 */
function updb() {
  updatedb();
}

/**
 * Apply database updates.
 *
 * @return bool
 *   TRUE on success, otherwise FALSE.
 */
function updatedb() {
  // Check if there is a connection.
  if (!check_connection()) {
    print "No connection found.\n";
    return FALSE;
  }

  // Load the base URL to the remote drupal website from the cache.
  if (!$base_url = cache_get('base_url')) {
    print "No base URL found.\n";
    return FALSE;
  }

  // Retrieve the form token from the update form.
  $start_update_result = do_request($base_url . "/update.php?op=info");
  if (!preg_match('@op=([^"]*)@', $start_update_result, $start_update_matches)) {
    print "Failed retrieving update form token.";
    return FALSE;
  }
  // Remove encoding.
  $update_form_continue = str_replace('amp;', '', $start_update_matches[0]);

  // Submit the update form and retrieve the form token to apply the updates.
  $continue_result = do_request($base_url . '/update.php?' . $update_form_continue);
  if (!preg_match('@form_token" *value="([^"]*)"@', $continue_result, $form_token_match)) {
    print "Failed retrieving apply update form token.\n";
    return FALSE;
  }
  // Also, look for the 'Apply updates' submit button.
  if (!preg_match('@\<input type="submit" id="edit-submit" name="op" value="([^"]*)" class="form-submit" \/\>@', $continue_result, $op_match)) {
    print "No updates available.\n";
    return FALSE;
  }
  $update_form_token = $form_token_match[1];
  $op = $op_match[1];

  // Look for updates to apply.
  if (!preg_match_all('@"start\[([^"]*)\]" *value="([^"]*)"@', $continue_result, $update_matches)) {
    print "No updates found.";
    return FALSE;
  }

  // Compose post data to start the updates.
  $update_form_fields = array();
  for ($i = 0; $i < count($update_matches[1]); $i++) {
    $update_form_fields[] = "start[" . $update_matches[1][$i] . "]=" . $update_matches[2][$i];
  }
  $update_form_fields[] = "form_id=update_script_selection_form";
  $update_form_fields[] = "op=$op";
  $update_form_fields[] = "form_token=$update_form_token";

  // Submit the 'Apply updates' form.
  $init_updates_result = do_request($base_url . '/update.php?' . $update_form_continue, implode("&", $update_form_fields));
  // Extract the refresh URL to manually start the update process.
  if (!preg_match('@equiv="Refresh" *content="0; URL=([^"]*)"@', $init_updates_result, $apply_updates_match)) {
    print "Failed retrieving update process URL.\n";
    return FALSE;
  }

  // Apply the updates.
  print "Applying database updates...\n";
  $apply_updates_result = do_request(str_replace('amp;', '', $apply_updates_match[1]));
  // Look for the status message.
  if (preg_match('@\<div class="message"\>([^"]*)\<br \/\>@', $apply_updates_result, $update_result_match)) {
    print trim($update_result_match[1]) . "\n";
  }
  else {
    print "Unknown status.\n";
  }
  // Extract the refresh URL to finish the update process manually.
  if (!preg_match('@equiv="Refresh" *content="0; URL=([^"]*)"@', $apply_updates_result, $finish_updates_match)) {
    print "Failed retrieving finish update process URL. Please check your website.\n";
  }

  // Finish the update process.
  $finish_updates_result = do_request(str_replace('amp;', '', $finish_updates_match[1]));
  if (preg_match('@\<div id="update-results">(.*)\<\/div\>@sU', $finish_updates_result, $update_results_match)) {
    print "---\n";
    print trim($update_results_match[1]) . "\n";
    print "---\n";
  }
  print "Finished applying updates.\n";

  return TRUE;
}

/**
 * Alias for @feature_revert_all.
 */
function fra() {
  feature_revert_all();
}

/**
 * Revert all features.
 *
 * @return bool
 *   TRUE on success, otherwise FALSE.
 */
function feature_revert_all() {
  // Check if there is a connection.
  if (!check_connection()) {
    print "No connection found.\n";
    return FALSE;
  }

  // Get the base URL from the cache.
  if (!$base_url = cache_get('base_url')) {
    print "No base URL found.\n";
    return FALSE;
  }

  // Load all features.
  $result = do_request($base_url . "/admin/structure/features");
  if (!preg_match_all('@\<a href="([^"]*)" class="admin-overridden features-storage"\>@', $result, $matches)) {
    print "No overridden features found.\n";
    return FALSE;
  }

  print "Initiating feature revert all. Found " . count($matches[1]) . " active features.\n";

  // Count the number of reverted features.
  $revert_count = 0;
  // Iterate through the features.
  for ($i = 0; $i < count($matches[1]); $i++) {
    print "Checking feature " . str_replace("/admin/structure/features/", "", $matches[1][$i]) . "... ";
    // Load the feature page.
    $feature_result = do_request($base_url . $matches[1][$i]);
    // Check if there is a revert button, if a form token is present and if
    // there are checkboxes to revert specific feature components.
    if (
      preg_match('@id="edit-buttons-revert"@', $feature_result, $overridden_matches) &&
      preg_match('@form_token" *value="([^"]*)"@', $feature_result, $token_matches) &&
      preg_match_all('@\<input type="checkbox" id=".*" name="([^"]*)"@', $feature_result, $input_matches)
    ) {
      // Put all feature components in the post data.
      $postdata = array();
      for ($j = 0; $j < count($input_matches[1]); $j++) {
        $postdata[] = $input_matches[1][$j] . "=1";
      }
      // Perform the revert request.
      $revert_request = do_request($base_url . $matches[1][$i], implode("&", $postdata) . "&op=Revert components&form_id=features_admin_components&form_token=" . $token_matches[1]);
      // Look for a status message.
      if (preg_match('@\<div class="messages status"\>.*\<h2.*\>.*\<\/h2\>(.*)\<\/div\>@sU', $revert_request, $status_matches)) {
        if (isset($status_matches[1])) {
          print trim($status_matches[1]) . "\n";
        }
        else {
          print "Sent revert request, unknown status.";
        }
      }
      $revert_count++;
    }
    else {
      print "Matching defaults.";
    }
    print "\n";
  }

  print "---\n";
  print "Finished reverting features. Reverted " . $revert_count . " feature(s).\n";
}

/**
 * Alias for @cache_clear.
 */
function cc() {
  cache_clear();
}

/**
 * Clear all caches.
 *
 * @return bool
 *   TRUE on success, otherwise FALSE.
 */
function cache_clear() {
  // Check if there is a connection.
  if (!check_connection()) {
    print "No connection found.\n";
    return FALSE;
  }

  // Get the base URL from the cache.
  if (!$base_url = cache_get('base_url')) {
    print "No base URL found.\n";
    return FALSE;
  }

  // Load the performance page.
  $form_result = do_request($base_url . "/admin/config/development/performance");
  // Look for the clear cache button and the form token.
  if (
    preg_match('@\<input type="submit" id="edit-clear" name="op" value="([^"]*)" class="form-submit" \/\>@', $form_result, $cache_button_match) &&
    preg_match('@form_token" *value="([^"]*)"@', $form_result, $token_match)
  ) {
    $op = $cache_button_match[1];
    $form_token = $token_match[1];

    print "Initiating cache clear all...\n";

    // Trigger the cache clear request.
    $clear_result = do_request($base_url . "/admin/config/development/performance", "op=$op&form_id=system_performance_settings&form_token=$form_token");
    // Look for a status message.
    if (preg_match('@\<div class="messages status"\>.*\<h2.*\>.*\<\/h2\>(.*)\<\/div\>@sU', $clear_result, $status_match)) {
      if (isset($status_match[1])) {
        print trim($status_match[1]) . "\n";
      }
      else {
        print "Sent cache clear request, unknown status.\n";
      }
    }

    return TRUE;
  }

  print "Failed clearing all caches. No cache clear button or form token found.\n";

  return FALSE;
}

/**
 * Execute a cURL request.
 *
 * @param string $url
 *   A full URL to connect with.
 * @param string $postdata
 *   Data to pass with the post request.
 *
 * @return string
 *   Response HTML from the requested website.
 */
function do_request($url, $postdata = "") {
  global $config;
  $cache = cache_get();

  // Start the cURL request.
  $crl = curl_init();
  curl_setopt($crl, CURLOPT_URL, $url);
  // Add htpasswd info if available in the cache.
  if (!empty($cache['http_user']) && !empty($cache['http_pass'])) {
    curl_setopt($crl, CURLOPT_USERPWD, $cache['http_user'] . ":" . $cache['http_pass']);
    curl_setopt($crl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  }
  // Read and save the session.
  curl_setopt($crl, CURLOPT_COOKIEFILE, $config['drupal_cookie_file']);
  curl_setopt($crl, CURLOPT_COOKIEJAR, $config['drupal_cookie_file']);
  curl_setopt($crl, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
  // Send postdata if there is any.
  if (!empty($postdata)) {
    curl_setopt($crl, CURLOPT_POST, 1);
    curl_setopt($crl, CURLOPT_POSTFIELDS, $postdata);
  }
  // Prevent screen output.
  ob_start();
  $result = curl_exec($crl);
  ob_end_clean();

  // Log errors.
  if ($errno = curl_errno($crl)) {
    print curl_strerror($errno);
    $result = "";
  }
  curl_close($crl);

  return $result;
}
