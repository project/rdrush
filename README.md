# Remote Drush
Remote drush (rdrush) offers drush functionality for drupal websites without SSH access.

## Not server-side
Because rdrush doesn't use any actual drush functionality and doesn't need access to the database or code, it can be executed on the client side.

## cURL
All commands and actions are executed via cURL requests. To make this work, rdrush needs the login credentials of an admin user in the remote drupal environment.
It creates a user session and performs all drush-like commands via form requests and submits.

## Shared hosting
Rdrush aims to bring drush functionality to websites which are inaccessible via the server. Shared hosting companies, for example, mostly do not allow users
to access the server via commandline. All the user gets is FTP access and a PHPMyAdmin panel.
Rdrush now allows these users to utilize the most basic drush commands like applying database updates and cache clearing from where ever they like.

# Requirements
- PHP > 5.3
- PHP cURL
- A drupal admin account

# Installation
Rdrush needs no installation. Just put rdrush.php anywhere you like and you're good to go.

# Usage
To perform actions in a drupal site, rdrush needs to log in to that site as an admin user.
```
# Set up a connection.
php rdrush.php connect http://www.yourdrupalsite.com drupal_username drupal_password
# Now you're ready to use rdrush.
php rdrush.php cc all
# Don't forget to disconnect to remove locally stored session data.
php rdrush.php disconnect
````
Enter `php rdrush.php help` to see a list of available commands.

# Note
Because rdrush actually impersonates a drupal user and has no direct access to the database, there are a lot of original drush functions which rdrush just can not copy.
However, despite the restrictions, I try to stay true to the original drush and provide as much functionality as possible to make everybody's lives a little bit easier.
I cannot do this alone, though, so any help is appreciated. :-)
